import * as express from "express"

const app = express()

app.get("/", (req, res) => {
    res.type("html").send("Hello world!")
})

app.listen(8080, () => {
    console.log("Server started on port 8080")
})
FROM node:8-alpine

WORKDIR /app
COPY . .

RUN npm prune --production

EXPOSE 8080
CMD npm start